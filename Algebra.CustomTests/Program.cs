﻿using System;
using static Algebra.Functions;

Console.WriteLine($"{(F_Tests(-13.45, 13.45, 0.00001) ? "Test passed." : "Test failed.")}");
Console.WriteLine($"{(F_Tests(-1000.12, 1000.12, 0.00001) ? "Test passed." : "Test failed.")}");
Console.WriteLine($"{(F_Tests(1, 1, 0.00001) ? "Test passed." : "Test failed.")}");
Console.WriteLine($"{(F_Tests(1.2, 1.44,0.00001) ? "Test passed." : "Test failed.")}");
Console.WriteLine($"{(F_Tests(2, 4, 0.00001) ? "Test passed." : "Test failed.")}");
Console.WriteLine($"{(F_Tests(78.45, 4, 0.00001) ? "Test passed." : "Test failed.")}");

bool F_Tests(double x, double expected, double accuracy) => Math.Abs(F(x) - expected) <= accuracy;
// {
//     // double actual = F(x);
//     // //bool accept = Math.Abs(actual - expected) <= accuracy ? true : false;
//     // bool accept = Math.Abs(actual - expected) <= accuracy;
//     // return accept;
//
//     return Math.Abs(F(x) - expected) <= accuracy;
// }