﻿using System;

Console.WriteLine("Enter x:");
double x = double.Parse(Console.ReadLine()!);

Console.Clear();

// double result = 0;
// if (x <= 0)
// {
//     result = -x;
// }
// else if (x < 2)
// {
//     result = x * x;// Math.Pow(x, 2);
// }
// else
// {
//     result = 4;
// }
//Console.WriteLine($"f({x}) = {result}");

Console.WriteLine($"f({x}) = {Algebra.Functions.F(x)}");

return 0;