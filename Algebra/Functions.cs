﻿namespace Algebra
{
    public static class Functions
    {
        public static double F(double x)
        {
            double result;
        
            if (x <= 0)
            {
                result = -x;
            }
            else if (x < 2)
            {
                result = x * x;
            }
            else
            {
                result = 4;
            }

            return result;
        }
    }
}

